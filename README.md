This is an Gnome extension to remove the restriction of only being able to change one workspace at a time using touchpad gestures. It does this by enabling long gesture swipes, allowing you to fling through all open workspaces as fast as you like.

To install, clone this repository to your ```~/.local/share/gnome-shell/estensions```folder, ensuring that the parent folder name is ```flingit@whio.dev```. Log out and back in again in order to be able to enable the extension.
