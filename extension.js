import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';


export default class FlingItExtension extends Extension {
    enable() {
        //Enable long swipes
        Main.wm._workspaceAnimation._swipeTracker.allowLongSwipes = true
    }

    disable() {
        //Disable long swipes
        Main.wm._workspaceAnimation._swipeTracker.allowLongSwipes = false
    }

}

